﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObject/SpawnManagerScript", order = 1)]
public class ScriptableClass : ScriptableObject{

	public GameObject bombPrefab;

	public int BlowTime;
}
