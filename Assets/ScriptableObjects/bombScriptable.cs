﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class bombScriptable : ScriptableObject {

	public abstract void explode (MonoBehaviour bombScript);
}
