﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Player Menu/Ability")]
public class Ability : ScriptableObject {
	public float speedBoostTime = 0f;
	public float speedBoostValue = 0.0f;
	PlayerMovement playerObj;
	public void executeAbility(MonoBehaviour player) {
		Debug.Log (player.name + " Adding Speed");
		playerObj = player.gameObject.GetComponent<PlayerMovement>();
		playerObj.addSpeedTime (speedBoostValue, speedBoostTime);
	}
}
