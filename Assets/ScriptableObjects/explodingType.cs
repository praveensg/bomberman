﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Bomb Types/Timer bomb")]
public class explodingType : bombScriptable {

	public GameObject bombPrefab;
	public int explosionLength;
	
	public override void explode (MonoBehaviour bombScript)
	{
		var transform = bombScript.gameObject.transform;
		Vector3 dropPosition = new Vector3 (Mathf.RoundToInt (transform.position.x), Mathf.RoundToInt (transform.position.y),
			Mathf.RoundToInt (transform.position.z));
		GameObject bomb_instance = Instantiate (bombPrefab, dropPosition, Quaternion.identity);
		bomb_instance.GetComponent<Bomb> ().setParent (transform.GetComponent<PlayerBomb>());
		bomb_instance.GetComponent<Bomb> ().ExplosionLength = explosionLength;
	}
}
