﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public int playerNumber = 1;

	// speed along the axes
	private float speedX = 0.0f;
	private float speedY = 0.0f;

	// speed to be sent to the animation controller
	float speed;

	// boost speed value
	float speedAdder = 0.0f;
	float boostTime = 0.0f;

	// direction the character faces when pressing input
	Vector3 stickDirection;

	Animator playerAnim;


	private string horzAxisName;
	private string vertAxisName;
	private Rigidbody playerRigidBody;
	public Ability playerAbility;

	void Awake() {
		playerRigidBody = GetComponent<Rigidbody> ();
		playerAnim = GetComponent<Animator> ();
	}

	void OnEnable() {
		playerRigidBody.isKinematic = false;
		speed = 0.0f;
		stickDirection = Vector3.zero;
	}

	void Start() {
		horzAxisName = "Horizontal" + playerNumber;
		vertAxisName = "Vertical" + playerNumber;
	}


	void OnDisable() {
		if (playerAnim.isActiveAndEnabled)
			playerAnim.SetFloat ("Speed", 0.0f);
		playerRigidBody.isKinematic = true;
	}

	void Update() {
		speedX = Input.GetAxis (horzAxisName);
		speedY = Input.GetAxis (vertAxisName);
		speed = speedX + speedY;
		if (Mathf.Abs (speed) > 0) {
			speed = speed > 0 ? (speedAdder + Mathf.Min (1.0f, speed)) : (Mathf.Max (-1.0f, speed) - speedAdder);
		}
		playerAnim.SetFloat ("Speed", Mathf.Abs (speed));
	}

	void FixedUpdate() {
		stickDirection = new Vector3 (speedX, 0.0f, speedY);
		stickDirection.Normalize ();
		stickDirection.y = 0.0f;
		//		Debug.DrawRay (new Vector3 (transform.position.x, transform.position.y + 2.0f, transform.position.z), stickDirection, Color.blue);

		Quaternion toDirection;
		if (stickDirection == Vector3.zero) {
			stickDirection = transform.forward;
		}
		toDirection = Quaternion.LookRotation (stickDirection);
		transform.rotation = Quaternion.Slerp (transform.rotation, toDirection, 0.5f);
	}

	public void engageFlashMode() {
			playerAbility.executeAbility (this);
		StartCoroutine ("speedTimer");
	}

	public void addSpeedTime(float newSpeed, float newTime) {
		speedAdder = newSpeed;
		boostTime = newTime;
	}

	public void resetSpeed() {
		Debug.Log (transform.gameObject.name + " reset speed");
		speedAdder = 0.0f;
	}

	IEnumerator speedTimer() {
		Debug.Log (gameObject.name + " running timer");
		while (boostTime > 0) {
			yield return new WaitForSeconds (1.0f);
			--boostTime;
		}
		resetSpeed ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Explosions")) {
			StopAllCoroutines ();
			gameObject.SetActive (false);
		}
	}
}
