﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBomb : MonoBehaviour {

	public int playerNumber = 1;
	public GameObject bomb;
	public int maxBombCount = 3;
	public const int defaultBombCount = 3;
	private int currentBombCount;
	public int pickedupBombCnt = 0;
	public int additionalBombTimer = 0;
	public int explosionLengthTimer = 0;
	public int detonationTimer = 0;

	private bool detonationReached = false;

	public AudioSource gameAudio;
	string fireInput = "";

//	public List<testBomb> test;


	// use scriptable objects to set the length of the bombs and number
	public List<bombScriptable> expBombInstance;
	public int currentBombTypeIndex = 0;

	private GameObject currentDetonator = null;

	public GameObject CurrentDetonator {
		get {
			return currentDetonator;
		}
		set {
			currentDetonator = value;
		}
	}

	void Start() {
		fireInput = "Fire" + playerNumber;
		resetExtraBombs ();
		currentBombCount = defaultBombCount;
	}


	void Update() {
		if (Input.GetButtonDown (fireInput) || (CurrentDetonator != null && detonationReached)) {
			dropBomb ();
		}
	}

	public void dropBomb() {
		switch(currentBombTypeIndex) {
		case 0:
		case 2:
			{
				if (currentBombCount + pickedupBombCnt != 0) {
					expBombInstance [currentBombTypeIndex].explode (this);
//					Vector3 dropPosition = new Vector3 (Mathf.RoundToInt (transform.position.x), Mathf.RoundToInt (transform.position.y),
//						                       Mathf.RoundToInt (transform.position.z));
//					GameObject bomb_instance = Instantiate (bomb, dropPosition, Quaternion.identity);
//					bomb_instance.GetComponent<Bomb> ().setParent (this);
					--currentBombCount;
				}
			}
			break;
		case 1:
			{
				if (CurrentDetonator != null) {
					StopCoroutine ("resetBombType");
					CurrentDetonator.GetComponent<DetonatingBomb> ().overrideExplode ();
				} else {
					expBombInstance [currentBombTypeIndex].explode (this);
				}
			}
			break;
		}
	}

	public void incrementBomb() {
		++currentBombCount;
		currentBombCount = Mathf.Min (currentBombCount, maxBombCount + pickedupBombCnt);
	}

	public void setExtraBombs() {
		pickedupBombCnt = 2;
	}

	public void resetExtraBombs() {
		pickedupBombCnt = 0;
	}

	public void recheckBombInv() {
		currentBombTypeIndex = 0;
		CurrentDetonator = null;
		detonationReached = false;
	}

	public IEnumerator bombResetTimer() {
		int currentTime = additionalBombTimer;
		while (currentTime > 0) {
			yield return new WaitForSeconds (1.0f);
			--currentTime;
		}
		resetExtraBombs ();
	}

	public void increaseExplosionLength() {
		currentBombTypeIndex = 2;
	}

	public void resetExpLength() {
		StartCoroutine (resetExplosionLength ());
	}

	public IEnumerator resetExplosionLength() {
		
		int currentTime = explosionLengthTimer;
		while (currentTime > 0) {
			yield return new WaitForSeconds (1.0f);
			--currentTime;
		}
		currentBombTypeIndex = 0;
	}

	public void enableDetonatingBomb() {
		currentBombTypeIndex = 1;
		detonationReached = false;
	}

	public void resetBombTimer() {
		StartCoroutine (resetBombType ());
	}

	public IEnumerator resetBombType() {
		int currentTime = detonationTimer;
		while (currentTime > 0) {
			yield return new WaitForSeconds (1.0f);
			--currentTime;
		}
		if (CurrentDetonator != null)
			detonationReached = true;
		else
			currentBombTypeIndex = 0;
	}

	void OnDisable() {
		StopAllCoroutines ();
		currentBombTypeIndex = 0;
		CurrentDetonator = null;
		pickedupBombCnt = 0;
	}
}
