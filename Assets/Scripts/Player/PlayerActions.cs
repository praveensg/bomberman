﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {

	private Material playerMat;

	void OnEnable()
	{
		playerMat = transform.GetChild (0).GetComponent<Renderer> ().material;
		//SetPlayerColor ();
	}

	public void SetPlayerColor(Color playerColor) {
		playerMat.SetColor ("_Color", playerColor);
	}
}
