﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControl : MonoBehaviour {

	float speedX = 0.0f;
	float speedY = 0.0f;
	float speed;

	float speedAdder = 0.0f;
	string playerName;

	public string PlayerName {
		get {
			return playerName;
		}
		set {
			playerName = value;
		}
	}

	Vector3 stickDirection;
	Animator player1Anim;
	Player currentPlayer;

	// Use this for initialization
	void Start () {
		player1Anim = GetComponent<Animator> ();
		currentPlayer = GetComponent<Player> ();
		currentPlayer.enableBoost += speedBooster;
//		controlScript.Initialize (this);
	}

	void OnDestroy() {
		currentPlayer.enableBoost -= speedBooster;
	}

	// Update is called once per frame
	void Update () {
		speedX = Input.GetAxis ("Horizontal");
		speedY = Input.GetAxis ("Vertical");
		speed = speedX + speedY;
		speed = speed > 0 ? (speedAdder + Mathf.Min (1.0f, speed)) : (Mathf.Max (-1.0f, speed) - speedAdder);
		player1Anim.SetFloat ("Speed", Mathf.Abs (speed));

		if (Input.GetButtonDown("Fire1")) {
			GetComponent<PlayerBomb> ().dropBomb ();
		}
//		controlScript.control(this);

	}

	void FixedUpdate() {
		stickDirection = new Vector3 (speedX, 0.0f, speedY);
		stickDirection.Normalize ();
		stickDirection.y = 0.0f;
//		Debug.DrawRay (new Vector3 (transform.position.x, transform.position.y + 2.0f, transform.position.z), stickDirection, Color.blue);

		Quaternion toDirection;
		if (stickDirection == Vector3.zero) {
			stickDirection = transform.forward;
		}
		toDirection = Quaternion.LookRotation (stickDirection);
		transform.rotation = Quaternion.Slerp (transform.rotation, toDirection, 0.5f);
//		controlScript.physicsControl(this);
	}

	void speedBooster() {
		StartCoroutine ("BoostRoutine");
	}

	IEnumerator BoostRoutine() {
		speedAdder = 1.0f;
		int time = 5;
		while (time > 0) {
			Debug.Log ("In speed boost mode");
			yield return new WaitForSeconds (1.0f);
			--time;
		}
		speedAdder = 0.0f;
	}
}
