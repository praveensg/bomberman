﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour {

	public Transform player1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (player1) {
			transform.position = player1.position + (Vector3.forward * -8f) + (Vector3.up * 15f);
			transform.LookAt (player1);
		}
	}
}
