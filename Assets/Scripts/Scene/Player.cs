﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	uint lives = 3;

	public delegate void OnSpeedBoostEngaged();
	public OnSpeedBoostEngaged enableBoost;

	public uint Lives {
		get {
			return lives;
		}
		set {
			lives = value;
		}
	}

	Item slot1;
	uint itemCount;

	float speedAdder = 0.0f;

	public float SpeedAdder {
		get {
			return speedAdder;
		}
		set {
			speedAdder = value;
		}
	}

	// Use this for initialization
	void Start () {
//		slot1 = new Bomb(this);
		itemCount = 3;
	}
	
	// Update is called once per frame
	void Update () {
		//
		/*
		 * on pressing drop bomb,
		 * release a bomb if count is > 1
		 * decrement bomb
		 * when bomb explodes, increment the bomb counter
		 */
//		if (Input.GetKeyDown ("Fire1") && itemCount > 0) {
//			slot1.useItem(this, transform.position);
//			--itemCount;
//		}
	}

	void incrementBombs() {
		
	}

	public void engageFlashMode() {
		if (enableBoost != null)
			enableBoost();
	}


}
