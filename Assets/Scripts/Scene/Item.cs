﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour {

	Player owner;

	public Player Owner {
		get {
			return owner;
		}
		protected set {
			owner = value;
		}
	}

	public Item() {
		
	}

	public abstract void useItem (Player player, Vector3 position);
}
