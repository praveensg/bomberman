﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class is used the generate the overall scene with a bunch of blocks
 * and some indestructible and indestructible blocks
 */

// level holds information on the number of blocks present in the form of an array
// and assigns different types to different blocks

public class Level {

	Block [,] blocks;

	protected int width;

	public int Width {
		get {
			return width;
		}
	}

	protected int height;

	public int Height {
		get {
			return height;
		}
	}

	public Level (int width = 21, int height = 21) {
		this.width = width;
		this.height = height;
		blocks = new Block[width, height];

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				blocks [i,j] = new Block(i, j);
			}
		}
	}
}
