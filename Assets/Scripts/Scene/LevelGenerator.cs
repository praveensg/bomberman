﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	Level level;

	public Level Level {
		get {
			return level;
		}
	}

	public Sprite floorSprite;
	public GameObject cubePrefab;
	public GameObject destructibleObject;
	public GameObject plane;

	public GameObject playerModel;

	GameManager sceneManager;

	// Use this for initialization
	void Start () {
		level = new Level();
		sceneManager = GetComponent<GameManager> ();
		// create width x height number of game objects and position them accordingly
		for (int i = 0; i < level.Width; i++) {
			for (int j = 0; j < level.Height; ++j) {
//				GameObject currentBlock = new GameObject ();
//				currentBlock.name = "BlockAt" + "_" + i + "_" + j;
//
//				currentBlock.transform.position = new Vector3 (i, j, 0);
//				SpriteRenderer blockSprite = currentBlock.AddComponent<SpriteRenderer> ();
//				blockSprite.sprite = floorSprite;

				// instantiate the cubes
				if ((i == 0 || j == 0) || (i == level.Width - 1 || j == level.Height - 1) || (i % 2 == 0 && j % 2 == 0)) {
					GameObject currentCube = (GameObject)Instantiate (cubePrefab, new Vector3 (i, 0, j), Quaternion.identity);
					currentCube.name = "BlockAt" + "_" + i + "_" + j;
				}
			}
		}
		//spawnDestructible ();
		plane.SetActive (true);
		plane.transform.position = new Vector3 (level.Width / 2, -0.5f, level.Height / 2);

		//spawn characters
		//spawnCharacters();
	}

	public void spawnDestructible() {
		Vector3 spawnPos1 = sceneManager.SpawnPosition [0];
		Vector3 spawnPos2 = sceneManager.SpawnPosition [1];
		for (int i = 0; i < level.Width; ++i) {
			for (int j = 0; j < level.Height; ++j) {
				if (!(i % 2 == 0 && j % 2 == 0) && (spawnPos1.x != i && spawnPos1.z != j && spawnPos2.x != i && spawnPos2.z != j)) {
					float randomValue = Random.value;
					if (randomValue < 0.2f) {
						Instantiate (destructibleObject, new Vector3 (i, 0, j), Quaternion.identity);
					}
				}
			}
		}
	}

	public void clearBlocks() {
		GameObject[] destructibles;
		destructibles = GameObject.FindGameObjectsWithTag ("destructible");
		foreach (GameObject block in destructibles) {
			Destroy (block);
		}
		destructibles = GameObject.FindGameObjectsWithTag ("pickups");
		foreach (GameObject block in destructibles) {
			Destroy (block);
		}
	}

	void spawnCharacters() {
		int xPosition = 1 + 2 * Random.Range (0, level.Width / 4);
		int yPosition = 1 + 2 * Random.Range (0, level.Height / 2);
		playerModel.transform.position = new Vector3 (xPosition, -0.4f, yPosition);
//		playerModel.transform.Rotate (new Vector3 (0.0f, 0.0f, 0.0f));
		Camera.main.transform.position = playerModel.transform.position + (Vector3.forward * -8f) + (Vector3.up * 6f);
		Camera.main.transform.LookAt (playerModel.transform);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
