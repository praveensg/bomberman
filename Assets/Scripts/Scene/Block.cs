﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This is just a data class and holds information on where it is located in the world space
 * This is the basic buliding block of the whole world.
 * 
 * By creating this data class, we separate the visual element from the computations
 * and it makes it easy to scale
 * 
 * So this class does not derive from mono behavior
 */

// there can be three types of blocks
// 1. solid indestructible block
// 2. destructible block
// 3. floor block

// The block can also have powerups, and bombs dropped if it's a floor
public enum BlockType {floor, destructible, indestructible};

public class Block {

	protected BlockType type = BlockType.floor;

	public BlockType Type {
		get {
			return type;
		}
		set {
			type = value;
		}
	}

	// position information
	protected int xPosition;
	protected int yPosition;

	public Block (int xPos, int yPos) {
		this.xPosition = xPos;
		this.yPosition = yPos;
	}
}
