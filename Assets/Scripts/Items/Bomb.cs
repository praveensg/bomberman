﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

	PlayerBomb weaponParent;
	public GameObject explosion;
	public AudioSource gameAudio;
	public Renderer bombMesh;

	private Material bombMat;

	private int explosionLength;

	public int ExplosionLength {
		get {
			return explosionLength;
		}
		set {
			explosionLength = value;
		}
	}

	public float bombColorTiming = 0.2f;
	private float currentTime = 0.0f;

	private bool isExploding = false;

	float timer = 3f;
	float explosionDistance;

	public LayerMask blockMask;

	void OnDestroy() {
		weaponParent.incrementBomb ();
	}

	public void setParent(PlayerBomb fromParent) {
		weaponParent = fromParent;
	}

	void Start() {
		Invoke ("explode", 3.0f);
		bombMat = bombMesh.material;
	}

	void explode() {
		Instantiate (explosion, transform.position, Quaternion.identity);
		GetComponent<MeshRenderer> ().enabled = false;
		GetComponent<Collider> ().enabled = false;
		createExplosions ();
		Destroy (gameObject, 0.3f * (ExplosionLength/2));
	}

	// in each linear direction, check upto the length of explosion variable
	// explode until it reaches the length or it reaches a collider
	private void createExplosions() {
		isExploding = true;
		StartCoroutine(explodeInDirection(Vector3.forward));
		StartCoroutine(explodeInDirection(Vector3.back));
		StartCoroutine(explodeInDirection(Vector3.right));
		StartCoroutine(explodeInDirection(Vector3.left));
	}


	IEnumerator explodeInDirection(Vector3 direction) {
		for (int i = 1; i <= ExplosionLength; ++i) {
			RaycastHit hit;
			Physics.Raycast (transform.position, direction, out hit, i, blockMask);

			if (!hit.collider) {
				Instantiate (explosion, (transform.position + direction * i), Quaternion.identity);
			} else
				break;
			yield return new WaitForSeconds (0.2f);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (!isExploding && other.CompareTag ("Explosions")) {
			CancelInvoke ();
			createExplosions ();
			Destroy (gameObject, 0.3f);
		}
	}

	void Update() {
		UpdateColor ();
	}

	void UpdateColor() {
		currentTime += Time.deltaTime;
		if (currentTime > 0.2f) {
			Color nextColor = (bombMat.color == Color.red) ? Color.black : Color.red;
			bombMat.SetColor ("_Color", nextColor);
			currentTime = 0.0f;
		}
	}
}
