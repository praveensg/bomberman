﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destructibleBlock : MonoBehaviour {

	public GameObject[] dropList;
	public float[] dropChance;

	int getDropIndex() {
		float currentValue = Random.value;

		for (int i = 0; i < dropChance.Length; ++i) {
			if (currentValue < dropChance [i]) {
				return i;
			} else
				currentValue -= dropChance [i];
		}
		return (dropChance.Length - 1);
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.CompareTag ("Explosions")) {
			explode ();
		}
	}

	public void explode() {
		GetComponent<MeshRenderer> ().enabled = false;
		GetComponent<BoxCollider> ().enabled = false;
		StartCoroutine (dropItem ());
	}

	IEnumerator dropItem() {
		yield return new WaitForSeconds (1.0f);
		int dropIndex = getDropIndex ();
		Instantiate (dropList [dropIndex], transform.position, Quaternion.identity);
		Destroy (gameObject);
	}
}
