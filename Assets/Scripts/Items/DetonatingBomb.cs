﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetonatingBomb : MonoBehaviour {

	PlayerBomb weaponParent;
	public GameObject explosion;

	private int explosionLength;

	public int ExplosionLength {
		get {
			return explosionLength;
		}
		set {
			explosionLength = value;
		}
	}


	private bool isExploding = false;

	[Header ("Bomb Tick Time")]
	public float timer = 10.0f;

	float explosionDistance;

	public LayerMask blockMask;

	void OnDestroy() {
//		weaponParent.incrementBomb ();
		weaponParent.recheckBombInv();
	}

	public void setParent(PlayerBomb fromParent) {
		weaponParent = fromParent;
	}

	void Start() {
		Invoke ("explode", timer);
	}

	public void explode() {
		Instantiate (explosion, transform.position, Quaternion.identity);
		GetComponent<MeshRenderer> ().enabled = false;
		GetComponent<Collider> ().enabled = false;
		createExplosions ();
		Destroy (gameObject, 0.3f);
	}

	// in each linear direction, check upto the length of explosion variable
	// explode until it reaches the length or it reaches a collider
	private void createExplosions() {
		isExploding = true;
		StartCoroutine(explodeInDirection(Vector3.forward));
		StartCoroutine(explodeInDirection(Vector3.back));
		StartCoroutine(explodeInDirection(Vector3.right));
		StartCoroutine(explodeInDirection(Vector3.left));
	}


	IEnumerator explodeInDirection(Vector3 direction) {
		for (int i = 1; i <= ExplosionLength; ++i) {
			RaycastHit hit;
			Physics.Raycast (transform.position, direction, out hit, i, blockMask);

			if (!hit.collider) {
				Instantiate (explosion, (transform.position + direction * i), Quaternion.identity);
			} else
				break;
			yield return new WaitForSeconds (0.2f);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (!isExploding && other.CompareTag ("Explosions")) {
			callExplode ();
		}
	}

	public void overrideExplode() {
		if (!isExploding) {
			callExplode ();
		}
	}

	void callExplode() {
		CancelInvoke ();
		Instantiate (explosion, transform.position, Quaternion.identity);
		createExplosions ();
		Destroy (gameObject, 0.3f);
	}
}
