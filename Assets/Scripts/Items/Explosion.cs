﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public AudioSource explosionAudio;

	// Use this for initialization
	void Start () {
		Invoke ("afterExplosion", 1f);
	}

	void afterExplosion() {
		Destroy (gameObject);
	}

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("destructible")) {
			other.GetComponent<destructibleBlock> ().explode ();
		}
	}
}
