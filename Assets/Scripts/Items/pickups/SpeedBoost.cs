﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : Item {

	private float boostValue;

	public float BoostValue {
		get {
			return boostValue;
		}
		set {
			boostValue = value;
		}
	}

	void OnTriggerEnter (Collider collidedPlayer) {
		if (collidedPlayer.CompareTag ("Explosions")) {
			Destroy (gameObject);
		} else if (collidedPlayer.CompareTag("Players")){
			var movementControl = collidedPlayer.transform.GetComponent<PlayerMovement> ();
			collidedPlayer.transform.GetComponent<PlayerMovement> ().engageFlashMode ();
			Destroy (gameObject);
		}
	}

	public override void useItem(Player player, Vector3 position) {

	}
}
