﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class additionalExplosionLength : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Players")) {
			var bombScript = other.GetComponent<PlayerBomb> ();
			bombScript.increaseExplosionLength ();
			bombScript.resetExpLength();
			Destroy (gameObject);
		} else if (other.CompareTag ("Explosions"))
			Destroy (gameObject);
	}
}
