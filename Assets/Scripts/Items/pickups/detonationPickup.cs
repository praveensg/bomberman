﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detonationPickup : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Players")) {
			var bombScript = other.GetComponent<PlayerBomb> ();
			if (bombScript.currentBombTypeIndex != 1) {
				bombScript.enableDetonatingBomb ();
				bombScript.resetBombTimer ();
				Destroy (gameObject);
			}
		} else if (other.CompareTag ("Explosions"))
			Destroy (gameObject);
	}
}
