﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class additionalBombs : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Players")) {
			var bombScript = other.GetComponent<PlayerBomb> ();
			bombScript.setExtraBombs ();
			StartCoroutine (bombScript.bombResetTimer ());
			Destroy (gameObject);
		} else if (other.CompareTag ("Explosions"))
			Destroy (gameObject);
	}
}
