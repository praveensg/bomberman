﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class playerManager{

	// The following members need to be accessed by the game manager to initialize the
	// player with a player number and a color to enable the appropriate controls
	public Color playerColor;
	[HideInInspector] public int playerNumber;
	[HideInInspector] public GameObject playerInstance;
	[HideInInspector] public int numberOfWins;

	private PlayerMovement movementScript;
	private PlayerBomb shootingScript;
	private PlayerActions playerProperty; // to change the color
	

	public void Initialize() {
		movementScript = playerInstance.GetComponent<PlayerMovement> ();
		shootingScript = playerInstance.GetComponent<PlayerBomb> ();
		playerProperty = playerInstance.GetComponent<PlayerActions> ();
		movementScript.playerNumber = playerNumber;
		shootingScript.playerNumber = playerNumber;
		playerProperty.SetPlayerColor (playerColor);
	}

	public void ActivateControls() {
		movementScript.enabled = true;
		shootingScript.enabled = true;
	}

	public void DeactivateControls() {
		movementScript.enabled = false;
		shootingScript.enabled = false;
	}
}
