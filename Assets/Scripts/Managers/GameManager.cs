﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public playerManager[] players;
	public GameObject playerObject;
	public LevelGenerator levelGenScript;
	public Text gameUI;
	private WaitForSeconds pauseTimeBeforeStart;
	private WaitForSeconds pauseTimeAfterEnd;

	public float startPauseTime;
	public float endPauseTime;
	public Button startButton;
	public Button restartButton;
	public GameObject uiPanel;

	private List<Vector3> spawnPosition;

	public List<Vector3> SpawnPosition {
		get {
			return spawnPosition;
		}
		set {
			spawnPosition = value;
		}
	}

	private Level currentLevel;

	private int GameRoundNumber = 0;
	private playerManager roundWinner;
	private playerManager gameWinner;

	private bool isGameStarted = false;
	private bool roundOver = false;

	public int TotalRounds = 3;
	public int roundTime = 1; // time in minutes;
	private IEnumerator roundTimerRoutine;
	public Text timerUI;

	void Start () {
		pauseTimeBeforeStart = new WaitForSeconds (startPauseTime);
		pauseTimeAfterEnd = new WaitForSeconds (endPauseTime);
		currentLevel = GetComponent<LevelGenerator> ().Level;
		// spawn the players and start the games
		spawnPosition = new List<Vector3> ();
		levelGenScript = GetComponent<LevelGenerator> ();
		spawnPlayers();
		roundTimerRoutine = countRoundTimer ();
		StartCoroutine (GameRoutine ());

	}

	void spawnPlayers() {
		int levelHeight = currentLevel.Height;
		int levelWidth = currentLevel.Width;

		int xPosition = 1 + 2 * Random.Range (0, levelWidth / 4);
		int yPosition = 1 + 2 * Random.Range (0, levelHeight / 2);

		spawnPosition.Add(new Vector3 (xPosition, -0.4f, yPosition));

		xPosition = 1 + 3 * (Random.Range (0, levelWidth / 4));
		xPosition = (xPosition % 2 == 1) ? xPosition : xPosition - 1;
		yPosition = 1 + 3 * (Random.Range (0, levelHeight / 4));
		yPosition = (yPosition % 2 == 1) ? yPosition : yPosition - 1;

		spawnPosition.Add(new Vector3 (xPosition, -0.4f, yPosition));

		for (int i = 0; i < players.Length; ++i) {
			players [i].playerInstance = Instantiate (playerObject, spawnPosition[i], Quaternion.identity) as GameObject;
			players [i].playerNumber = i + 1;
			players [i].Initialize ();
		}
	}

	public void SetGameStarted() {
		isGameStarted = true;
	}

	IEnumerator GameRoutine() {
		yield return StartCoroutine (StartRound ());
		yield return StartCoroutine (Gameplay ());
		yield return StartCoroutine (EndRoutine ());

		if (GameRoundNumber < TotalRounds)
			StartCoroutine (GameRoutine ());
	}


	IEnumerator StartRound() {
		++GameRoundNumber;
		DeactivateAllPlayers ();
		while (!isGameStarted) {
			yield return null;
		}
		isGameStarted = false;
		gameUI.text = "";
		hideButtons ();
		respawnPlayers ();
		levelGenScript.clearBlocks ();
		levelGenScript.spawnDestructible ();
		yield return pauseTimeBeforeStart;
		timerUI.gameObject.SetActive (true);
		StartCoroutine ("countRoundTimer");
	}

	IEnumerator Gameplay() {
		roundOver = false;
		ActivateAllPlayers ();
		while (!roundOver && !isAnyPlayerDead ()) {
			yield return null;
		}
	}


	IEnumerator EndRoutine() {
		StopCoroutine ("countRoundTimer");
		timerUI.gameObject.SetActive (false);
		yield return pauseTimeAfterEnd;
		DeactivateAllPlayers ();
		UpdateScore ();
		//gameUI.text = "Player";
		uiPanel.SetActive(true);
		if (GameRoundNumber < TotalRounds)
			restartButton.gameObject.SetActive(true);
		yield return null;
	}

	void UpdateScore() {
		int player1Win = 0;
		int player2Win = 0;
		string currentText = "";
		if (players [0].playerInstance.activeSelf) {
			player1Win = 1;
			players [0].numberOfWins += 1;
		}
		if (players [1].playerInstance.activeSelf) {
			player2Win = 1;
			players [1].numberOfWins += 1;
		}
		if (player1Win == player2Win) {
			currentText = "DRAW";
		} else if (player1Win == 1)
			currentText = "Player1 Won";
		else
			currentText = "Player2 Won";
		currentText += "\n" + "Player 1 - " + players [0].numberOfWins;
		currentText += "\n" + "Player 2 - " + players [1].numberOfWins;
		if (GameRoundNumber == TotalRounds)
			currentText += "\n" + "Game Over";
		gameUI.text = currentText;
	}

	void respawnPlayers() {
		players [0].playerInstance.SetActive (true);
		players [0].playerInstance.transform.position = SpawnPosition [0];
		players [1].playerInstance.SetActive (true);
		players [1].playerInstance.transform.position = SpawnPosition [1];
	}


	void ActivateAllPlayers() {
		for (int i = 0; i < players.Length; ++i) {
			players [i].ActivateControls ();
		}
	}

	void DeactivateAllPlayers() {
		for (int i = 0; i < players.Length; ++i) {
			players [i].DeactivateControls ();
		}
	}

	bool isAnyPlayerDead() {
		for (int i = 0; i < players.Length; ++i) {
			if (!players [i].playerInstance.activeSelf)
				return true;
		}
		return false;
	}

	void findRoundWinner() {
		for (int i = 0; i < players.Length; ++i) {
			
		}
	}

	void hideButtons() {
		restartButton.gameObject.SetActive(false);
		startButton.gameObject.SetActive(false);
		uiPanel.SetActive (false);
	}

	IEnumerator countRoundTimer() {
		int currentTime = roundTime * 60;
		int minutes = roundTime;
		int seconds = 0;
		string timeText = "";
		while (minutes >= 0 && seconds >= 0) {
			if (seconds == 0) {
				minutes--;
				seconds = 59;
			} else
				--seconds;
			timeText = minutes + ":";
			if (seconds < 10)
				timeText += "0";
			timeText += seconds;
			timerUI.text = timeText;
			yield return new WaitForSeconds(1.0f);
		}
		roundOver = true;
	}
}
